---
layout: page
title: About
subtitle: What is freedesktop-sdk, how can i get involved?
permalink: /about/
---

<h2> What is freedesktop-sdk? </h2>

The Freedesktop SDK project is a free and open source project that provides Platform and SDK runtimes for
[Flatpak](https://flatpak.org) apps and runtimes based on Freedesktop modules.
It was originally started as a [Flatpak subproject](https://github.com/flatpak/freedesktop-sdk-images)
to create a basic-neutral runtime. It is now a separate project, similar to
what happened with the GNOME and KDE runtimes.

As explained in [Alex's blog post](https://blogs.gnome.org/alexl/2018/05/16/introducing-1-8-freedesktop-runtime/) there has long been a desire to upgrade and modernize the SDK, which is where this specific project comes into the story.

This is a diagram of how the Freedeskop SDK runtimes interact with the rest of the Flatpak ecosystem:

[![diagram.png](https://s33.postimg.cc/fpqffim4f/diagram.png)](https://postimg.cc/image/huasglnqz/)

Along side Flatpak applications we are exploring how to use runtime in other ways, such as providing a minimal bootable system for anyone to use in their own project and bootable VM images which will use the freedesktop-sdk as a base.

<h2> How can I get involved? </h2>

We are active on IRC over at: <strong>#freedesktop-sdk</strong> on freenode

We have a mailing list [here](https://lists.freedesktop.org/mailman/listinfo/freedesktop-sdk)

We are a completely open project and welcome anyone to raise an issue or contribute! You can find our issue tracker [here](https://gitlab.com/freedesktop-sdk/freedesktop-sdk/issues). We welcome anyone to the project, if you can not find an issue please join us on IRC and we will be more than happy to help!
