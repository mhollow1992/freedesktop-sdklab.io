
### Start in 4 steps

1. Download or clone repo
2. Enter the folder: `cd website`
3. Install Ruby gems: `bundle install`
4. Start Jekyll server: `bundle exec jekyll serve`

Access, [localhost:4000/website](http://localhost:4000/website)

### Copyright and license

It is under [the MIT license](/LICENSE).

With a huge thanks to https://github.com/nandomoreirame/dotX, which this website is built on top of!

And also Marcia Ramos for the Gitlab pages guide: https://gitlab.com/jekyll-themes/default-bundler/blob/master/
